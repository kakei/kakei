with import (fetchTarball https://github.com/NixOS/nixpkgs/archive/057f9aecfb71c4437d2b27d3323df7f93c010b7e.tar.gz) {};
rustPlatform.buildRustPackage rec {
  pname = "kakei";
  version = "0.1.0";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [
    rustc
    cargo
    rustfmt
    rust-analyzer
  ];
}
